<?php

/**
 * @file
 * Tests for tickspot.module.
 */

namespace Drupal\tickspot\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the tickspot module.
 */
class tickspotTest extends WebTestBase {

  public static function getInfo() {
    return array(
      'name' => 'tickspot functionality',
      'description' => 'Test Unit for module tickspot.',
      'group' => 'Other',
    );
  }

  function setUp() {
    parent::setUp();
  }

  /**
   * Tests tickspot functionality.
   */
  function testtickspot() {
    //Check that the basic functions of module tickspot.
    $this->assertEqual(TRUE, TRUE, 'Test Unit Generated via Console.');
  }

}
