<?php

namespace Drupal\tickspot\Controller;

use Drupal\tickspot\Model\TickspotAPI;
use Drupal\tickspot\Model\TickspotClient;
use Drupal\Core\Controller\ControllerBase;

class DefaultController extends ControllerBase {

  // @todo configure in settings
  // const HOURLY_RATE = 75.0;
  // const CURRENCY = '€';
  // const SHOW_CLOSED_TASKS = FALSE;

  // @todo try - catch for TickspotException using the error code to handle messages translation with t()
  // @todo provide filter per date feature and default date values
  
  /**
   * @var TickspotAPI
   */
  private $tick;
  
  /**
   * Default constructor
   * 
   * @todo check if no settings are defined right after install
   */
  public function __construct() {
    // @todo find out how to test if values are set
    // causes Fatal error: Can't use method return value in write context 
    $config = \Drupal::config('tickspot.settings');
    $api_endpoint = $config->get('endpoint');
    $api_user_email = $config->get('email');
    $api_user_pwd =  $config->get('passwd');
    if(empty($api_endpoint) || empty($api_user_email) || empty($api_user_pwd)){
      drupal_set_message(t('Please configure <a href="@url">Tickspot API credentials.</a>'), array('@url' => 'admin/config/tickspot/credentials'));
    }
    $this->tick = new TickspotAPI($api_endpoint, $api_user_email, $api_user_pwd);
  }
  
  /**
   * Shows the task, grouped by projects for the current logged in client.
   * A subset of tasks could be defined (e.g. maintenance) consists of chosen tasks entries in a client's project.
   *  
   * @todo review security for the current user 
   * (do not show up projects and tasks that does not belong to the current user)
   * 
   * @global user $user
   * @param  integer $client_id 
   * @return string
   */
  public function client_projects_tasks_view($client_id) {
    
    global $user;
    
    $show_projects = FALSE;
    // if is client check if the client id matches
    if(in_array('client', $user->getRoles())){
      $cur_client_tickspot_id = TickspotClient::get_current_user_tickspot_client_id($user);
      if(!empty($cur_client_tickspot_id) && (int) $cur_client_tickspot_id === (int) $client_id){
        $show_projects = TRUE;
      }
    // @todo other roles than clients must be handled by permissions
    }elseif(in_array('administrator', $user->getRoles())){
      $show_projects = TRUE;
    }
    
    // iterates on projects and tasks and entries for the current client
    // @todo use api to render list
    $output = '';
    if($show_projects){
      $projects_xml = $this->tick->get_projects();
      foreach($projects_xml->project as $project){
        // filtering by current client id
        if((int) $project->client_id == (int) $client_id){
          $output .= '<h2>' . $project->name . '</h2>';
          $output .= '<ul>';
          // @todo filter by opened tasks if defined in settings
          foreach($project->tasks->task as $task){
            $output .= '<li>' . l($task->name, '/tickspot/client/task_entries/'. $client_id . '/'.  $project->id . '/'. $task->id);
            $output .= '<br>'.t('Overall total for this task').': <strong>'. $task->sum_hours .'</strong></li>';
          }
          $output .= '</ul>';
        }
      }
    }else{
      // @todo review wording
      $output .= t('You are not allowed to view the projects for this client ID.');
    }
    return $output;
  }
  
  /**
   * Shows the task entries for a task
   * 
   * @todo review security for the current user 
   * (do not show up task entries that does not belong to the current user)
   * 
   * @todo convert decimals in time or money
   * @todo include time or money display in the module settings
   * 
   * @param type $task_id
   * @return string
   */
  public function client_task_entries_view($client_id, $project_id, $task_id){
    $output = '';
    
    // example values, @todo expose and set default
    $dates['start_date'] = '2014-01-01';
    $dates['end_date'] = '2014-05-30';
    
    $options = array();
    $options['project_id'] = (int) $project_id;
    $options['task_id'] = (int) $task_id;
    $entries_xml = $this->tick->get_entries($dates, $options);
    
    $total_hours = 0.0;
    
    // @todo date format
    $output .= '<p>'. t('Start date') . ': '. $dates['start_date'];
    $output .= ' | ' . t('End date') . ': ' . $dates['end_date'] . '</p>';
    // @todo use api to render list
    if(!empty($entries_xml->entry)){
      $output .= '<h2>'.$entries_xml->entry[0]->task_name.' ('.$entries_xml->entry[0]->project_name.')</h2>';
      $output .= '<ul>';
      foreach ($entries_xml->entry as $entry) {
        $total_hours += (float) $entry->hours;
        $output .= '<li>' . $entry->notes . ': <strong>' . $entry->hours . '</strong></li>';
      }
      $output .= '</ul>';
      $output .= '<p>' . t('Total'). ': ' . $total_hours . '</p>';
    }else{
      $output .= '<p>'. t('No entries found for this task, for this period.') . '</p>';
    }
    $output .= '<p>'. l(t('Back to the projects'), '/tickspot/client/projects_tasks/'.$client_id) . '</p>';
    return $output;
  }
  
  /**
   * Shows the client list for the Tickspot account (owner) defined in the credentials form.
   * 
   * @return string
   */
  public function owner_clients_view() {
    $output = '';
    $clients_xml = $this->tick->get_clients();
    // @todo use api to render list
    $output .= '<ul>';
    foreach ($clients_xml->client as $client) {
      $output .= '<li>' . l($client->name, '/tickspot/client/projects_tasks/' . $client->id) . '</li>';
    }
    $output .= '</ul>';
    return $output;
  }
  
}
