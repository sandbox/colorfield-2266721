<?php

/**
 * @file
 * 
 * @todo define for role owner/administrator by default
 * 
 * Contains \Drupal\block_example\Plugin\Block\TickspotOwnerBlock.
 */

namespace Drupal\tickspot\Plugin\Block;

use Drupal\block\Annotation\Block;
use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Translation;

/**
 * Provides Tickspot owner menu.
 *
 * @Block(
 *   id = "tickspot_owner_block",
 *   subject = @Translation("Tickspot owner menu"),
 *   admin_label = @Translation("Owner menu")
 * )
 */
class TickspotOwnerBlock extends BlockBase {

  /**
   * Implements \Drupal\block\BlockBase::blockBuild().
   */
  public function build() {
    global $user;
    $markup = '';
    // @todo use list from api
    $markup .= '<ul>';
    $markup .= '<li>'.l('Clients', 'tickspot/owner/clients').'</li>';
    $markup .= '</ul>';
    return array(
      '#type' => 'markup',
      '#markup' => $markup,
    );
  }

}
