<?php

/**
 * @file
 * 
 * @todo define for role client by default
 * 
 * Contains \Drupal\block_example\Plugin\Block\TickspotClientBlock.
 */

namespace Drupal\tickspot\Plugin\Block;

use Drupal\tickspot\Model\TickspotClient;
use Drupal\block\Annotation\Block;
use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Translation;

/**
 * Provides Tickspot client menu.
 *
 * @Block(
 *   id = "tickspot_client_block",
 *   subject = @Translation("Tickspot client menu"),
 *   admin_label = @Translation("My account")
 * )
 */
class TickspotClientBlock extends BlockBase {

  /**
   * Overrides \Drupal\block\BlockBase::defaultConfiguration().
   * @todo just for testing purpose, could be deleted 
   */
  public function defaultConfiguration() {
    global $user;
    return array(
      'block_welcome_string' => '',
    );
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockForm().
   * @todo just for testing purpose, could be deleted 
   */
  public function blockForm($form, &$form_state) {
    $form['block_welcome_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Welcome message'),
      '#size' => 60,
      '#description' => t('This text will appear at the top of the client menu.'),
      '#default_value' => $this->configuration['block_welcome_string'],
    );
    return $form;
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockSubmit().
   * @todo just for testing purpose, could be deleted 
   */
  public function blockSubmit($form, &$form_state) {
    $this->configuration['block_welcome_string'] = $form_state['values']['block_welcome_text'];
  }

  /**
   * Implements \Drupal\block\BlockBase::blockBuild().
   */
  public function build() {
    global $user;
    $client_id = TickspotClient::get_current_user_tickspot_client_id($user);
    $markup = '';
    // @todo just for testing purpose, could be deleted 
    $markup .= $this->configuration['block_welcome_string'];
    // @todo use list from api
    $markup .= '<ul>';
    $markup .= '<li>'.l('Projects', '/tickspot/client/projects_tasks/' . $client_id).'</li>';
    $markup .= '<li>'.l('Profile', 'user').'</li>';
    $markup .= '</ul>';
    return array(
      '#type' => 'markup',
      '#markup' => $markup,
    );
  }

}
