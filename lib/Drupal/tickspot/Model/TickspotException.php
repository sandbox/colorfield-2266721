<?php

namespace Drupal\tickspot\Model;

/**
 * TickspotException
 * 
 * @todo review https://api.drupal.org/api/drupal/core!lib!Drupal!Core!EventSubscriber!ExceptionListener.php/class/ExceptionListener/8
 */
class TickspotException extends Exception {
  
  /**
   * Constructor, with mandatory message
   * 
   * @param type $message
   * @param type $code
   * @param Exception $previous
   */
  public function __construct($message, $code = 0, Exception $previous = null) {
    parent::__construct($message, $code, $previous);
  }

  /**
   * Overrides toString
   * 
   * @return type
   */
  public function __toString() {
    return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
  }
}
