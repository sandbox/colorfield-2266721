<?php

namespace Drupal\tickspot\Model;

// @todo review exception handling in Drupal 8

// use Drupal\tickspot\Model\TickspotException;

/**
 * Implements the Tickspot API.
 * Usage of all the methods assumes that the credentials passed contains an admin account.
 * 
 * This file should be imported as a library but is embeded in the module to simplify the development process.
 * 
 * @see http://www.tickspot.com/api
 * @see PHP implementation maintained at https://github.com/r-daneelolivaw/tickspot 
 */
class TickspotAPI {
  
  /**
   * Stores the credentials passed by the constructor.
   * @var array
   */
  private $credentials;
  
  /**
   * Default constructor
   * 
   * @param string $endpoint The Tickspot API url, e.g. https://company.tickspot.com/api
   * @param string $email
   * @param string $passwd
   * @throws TickspotException
   */
  public function __construct($endpoint = null, $email = null, $passwd = null) {
    $this->credentials = array();
    if($endpoint !== null && $email !== null && $passwd != null){
      $this->credentials['endpoint'] = $endpoint;
      $this->credentials['email'] = $email;
      $this->credentials['passwd'] = $passwd;
    }else{
      //throw new TickspotException('Provide the following credentials: https://company.tickspot.com/api, username and password.', 1);
    }
  }
    
  /**
	 * Destructor
	 */
	public function __destruct(){
    
	}
  
  /**
   * Helper that returns a xml file from a URL
   * 
   * @param string $url
   * @return SimpleXMLElement $xml
   */
  private function get_xml($url) {
    $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
    $xml_file = file_get_contents($url, false, $context);
    $xml = simplexml_load_string($xml_file);
    return $xml;
  }
  
  /**
   * Helper that builds the Tickspot URL
   * 
   * @param string $action
   * @param string $query_string
   * @return string $url
   * @throws TickspotException
   */
  private function get_api_url($action = null, $query_string = null) {
    $url = null;
    if($action !== null){
      if($query_string !== null){
        $url = $this->credentials['endpoint'] . '/' . $action;
        $url .= '?email=' . $this->credentials['email'] . '&amp;password=' . $this->credentials['passwd'] . '&amp;'. $query_string;
      }else{
        //throw new TickspotException('Provide a query string.', 3);
      }
    }else{
      //throw new TickspotException('Provide an action.', 2);
    }
    return $url;
  }
  
  /**
   * Helper that checks if the id is valid
   * 
   * @param type $id
   * @return boolean $is_valid
   */
  private function is_valid_id($id) {
    $is_valid = FALSE;
    if($id !== null && is_int($id)){
      $is_valid = TRUE;
    }
    return $is_valid;
  }
  
  /**
   * The clients method will return a list of all clients 
   * and can only be accessed by admins on the subscription.
   * 
   * @todo implement option
   * open [true|false]
   * 
   * @return string $xml;
   */
  public function get_clients() {
    $url = $this->get_api_url('clients', '');
    $xml = $this->get_xml($url);
    return $xml;
  }
    
  /**
   * The projects method will return projects filtered by the parameters provided. 
   * Admin can see all projects on the subscription, while non-admins can only access the projects they are assigned.
   * 
   * @todo implement options
   * project_id
   * open [true|false]
   * project_billable [true|false]
   * 
   * @param type $project_id
   * @return SimpleXMLElement $xml
   */
  public function get_projects($project_id = null) {
    $query_string = '';
    if($this->is_valid_id($project_id)){
      $query_string = 'project_id=' . $project_id;
    }
    $url = $this->get_api_url('projects', $query_string);
    $xml = $this->get_xml($url);
    return $xml;
  }
  
  /**
   * The tasks method will return a list of all the current tasks for a specified
   * project and can only be accessed by admins on the subscription.
   * 
   * @todo implement options
   * open [true|false]
   * task_billable [true|false]
   * 
   * @param int $project_id
   * @param int $task_id optional
   * @return SimpleXMLElement $xml
   */
  public function get_tasks($project_id = null, $task_id = null) {
    $query_string = '';
    if($this->is_valid_id($project_id)){
      $query_string = 'project_id=' . $project_id;
      if($this->is_valid_id($task_id)){
        $query_string .= '&amp;';
        $query_string .= 'task_id=' . $task_id;
      }
    }else{
      throw new Exception('Provide a valid project id.');
    }
    $url = $this->get_api_url('tasks', $query_string);
    $xml = $this->get_xml($url);
    return $xml;
  }
  
  /**
   * The method will return a list of all clients, projects, and tasks 
   * that are assigned to the user and available for time entries (open).
   * 
   * @return SimpleXMLElement xml
   */
  public function get_clients_projects_tasks() {
    $url = $this->get_api_url('clients_projects_tasks', '');
    $xml = $this->get_xml($url);
  }
  
  /**
   * The entries method will return a list of all entries that meet the 
   * provided criteria. Either a start and end date have to be provided
   * or an updated_at time. The entries will be in the start and end date range
   * or they will be after the updated_at time depending on what criteria 
   * is provided. Each of the optional parameters will further filter the response.
   * 
   * @todo handle timeout that can be produced with large range of dates
   * 
   * @todo implement options
   * project_id
   * task_id
   * user_id
   * user_email
   * client_id
   * entry_billable [true|false]
   * billed [true|false]
   * 
   * @param $dates [start_date, end_date|updated_at]
   * @param $options 
   * @return SimpleXMLElement xml
   * @throws Exception
   */
  public function get_entries($dates, $options) {
    $query_string = '';
    
    // mandatory dates
    if(!empty($dates['start_date']) && !empty($dates['end_date'])){
      $query_string = 'start_date=' . $dates['start_date'];
      $query_string .= '&amp;';
      $query_string .= 'end_date=' . $dates['end_date'];
    }else if(!empty($dates['updated_at'])){
      $query_string = 'updated_at=' . $dates['updated_at'];
    }else{
      throw new Exception('Provide a start and end date or an updated date.');
    }
    
    // optional parameters
    if($this->is_valid_id($options['project_id'])){
      $query_string .= '&amp;project_id=' . $options['project_id'];
      if($this->is_valid_id($options['task_id'])){
        $query_string .= '&amp;';
        $query_string .= 'task_id=' . $options['task_id'];
      }
    }
    $url = $this->get_api_url('entries', $query_string);
    $xml = $this->get_xml($url);
    return $xml;
  }
  
  /**
   * The users method will return a list of the most recently used tasks. 
   * This is useful for generating quick links for a user to select a task 
   * they have been using recently.
   * 
   * @return SimpleXMLElement xml
   */
  public function get_recent_tasks() {
    $url = $this->get_api_url('recent_tasks', '');
    $xml = $this->get_xml($url);
    return $xml;
  }
  
  
  /**
   * The users method will return a list of users.
   * 
   * @param type $project_id optional project id
   * @return SimpleXMLElement xml
   */
  public function get_users($project_id = null) {
    $query_string = '';
    if($this->is_valid_id($project_id)){
      $query_string = 'project_id=' . $project_id;
    }
    $url = $this->get_api_url('projects', $query_string);
    $xml = $this->get_xml($url);
    return $xml;
  }
  
  /**
   * The create_entry method will accept a time entry for a specified task_id 
   * and return the created entry along with the task and project stats.
   */
  public function create_entry() {
    // @todo to be implemented
  }
  
  /**
   * The update_entry method will allow you to modify attributes of an existing 
   * entry. The only required parameter is the id of the entry. 
   * Additional parameters must be provided for any attribute that you wish
   * to update. For example, if you are only changing the billed attribute, 
   * your post should only include the required parameters and the billed parameter.
   */
  public function update_entry() {
    // @todo to be implemented
  }
  
}