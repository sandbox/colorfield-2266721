<?php

namespace Drupal\tickspot\Model;

/**
 * Tickspot Client model and DAO
 *
 * @author christophe
 */
class TickspotClient {
  /**
   * Fetches the Tickspot client ID for the current logged in user
   * 
   * @todo define field_tickspot_client_id on install
   * @todo refactor on Model package
   * 
   * @param type $user
   * @return type
   */
  public static function get_current_user_tickspot_client_id($user){
    $query = db_select('users', 'u');
    $query->condition('u.uid', $user->id());
    $query->join('user__field_tickspot_client_id', 'tci', 'u.uid = tci.entity_id AND u.uid = :uid', array(':uid' => $user->id()));
    $query->fields('tci', array('field_tickspot_client_id_value'));
    $result = $query->execute()->fetchField();
    return $result;
  }
}
