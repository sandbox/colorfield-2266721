<?php

/**
 * @file
 * Contains \Drupal\user\CredentialsSettingsForm.
 */

namespace Drupal\tickspot;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Render\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure user settings for this site.
 */
class CredentialsSettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Constructs a \Drupal\tickspot\CredentialsSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandler $module_handler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tickspot_credentials_settings';
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, array &$form_state) {
    $config = $this->config('tickspot.settings');
    
    // Settings for anonymous users.
    $form['tickspot_settings'] = array(
      '#type' => 'details',
      '#title' => $this->t('Tickspot credentials'),
      '#open' => TRUE,
    );
    $form['tickspot_settings']['endpoint'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint API URL'),
      '#default_value' => $config->get('endpoint'),
      '#description' => $this->t('The custom API URL defined for your company, e.g. https://mycompany.tickspot.com/api.'),
      '#required' => TRUE,
    );
    $form['tickspot_settings']['email'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#default_value' => $config->get('email'),
      '#description' => $this->t('Email used for the Ticskpot account.'),
      '#required' => TRUE,
    );
    $form['tickspot_settings']['passwd'] = array(
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get('passwd'),
      '#description' => $this->t('Password used for the Ticskpot account.'),
      '#required' => TRUE,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, array &$form_state) {
    parent::submitForm($form, $form_state);

    $this->config('tickspot.settings')
      ->set('endpoint', $form_state['values']['endpoint'])
      ->set('email', $form_state['values']['email'])
      // @todo check if API is well used for security purpose
      ->set('passwd', $form_state['values']['passwd'])
      ->save();
  }

}
